package com.hybrid.library.configuration;

import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class TokenProperties {

    @Value("${application.name}")
    private String applicationName;

    @Value("${token.lifetime.milliseconds}")
    private int expiresIn;

    @Value("${token.secret}")
    private String secret;

    private String BEARER_PREFIX = "Bearer ";

    private String AUTH_HEADER = "Authorization";

    private SignatureAlgorithm signAlgorithm = SignatureAlgorithm.HS512;

    public TokenProperties() {
    }

    public String getApplicationName() {
        return applicationName;
    }

    public int getExpiresIn() {
        return expiresIn;
    }

    public String getSecret() {
        return secret;
    }

    public String getBearerPrefix() {
        return BEARER_PREFIX;
    }

    public String getAuthHeader() {
        return AUTH_HEADER;
    }

    public SignatureAlgorithm getSignAlgorithm() {
        return signAlgorithm;
    }

}
