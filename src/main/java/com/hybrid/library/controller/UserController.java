package com.hybrid.library.controller;

import com.hybrid.library.dto.BookCopyDTO;
import com.hybrid.library.dto.UserDTO;
import com.hybrid.library.mapper.UserMapper;
import com.hybrid.library.model.User;
import com.hybrid.library.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Email;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    private final UserService userService;

    private final UserMapper userMapper;

    public UserController(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<UserDTO> createUser(@Valid @RequestBody UserDTO user) {
        return new ResponseEntity<>(userMapper.mapToUserDTO(userService.createNewUser(userMapper.mapToUser(user))), HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<UserDTO> findUser(@PathVariable("id") Long id) {
        return ResponseEntity.ok(userMapper.mapToUserDTO(userService.findOne(id)));
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<Page<UserDTO>> findAllUsers(Pageable pageable) {
        return ResponseEntity.ok(userMapper.mapToUserDTOPage(userService.findAll(pageable)));
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteUser(@PathVariable("id") Long id) {
        userService.delete(id);
    }

    @PutMapping(value = "/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<UserDTO> updateUser(@PathVariable("id") Long id, @Valid @RequestBody User user) {
        return ResponseEntity.ok(userMapper.mapToUserDTO(userService.update(id, user)));
    }

    @GetMapping(value = "/search")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<UserDTO> findUserByEmail(@Email @RequestParam("email") String email) {
        return ResponseEntity.ok(userMapper.mapToUserDTO(userService.findByEmail(email)));
    }

}
