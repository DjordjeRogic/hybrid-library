package com.hybrid.library.controller;

import com.hybrid.library.dto.BookCopyDTO;
import com.hybrid.library.mapper.BookCopyMapper;
import com.hybrid.library.service.BookCopyService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/bookCopies")
public class BookCopyController {

    private final BookCopyService bookCopyService;

    private final BookCopyMapper bookCopyMapper;

    public BookCopyController(BookCopyService bookCopyService, BookCopyMapper bookCopyMapper) {
        this.bookCopyService = bookCopyService;
        this.bookCopyMapper = bookCopyMapper;
    }

    @PutMapping(value = "/{id}/return")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<BookCopyDTO> returnACopy(@PathVariable("id") Long id) {
        return ResponseEntity.ok(bookCopyMapper.mapToBookCopyDTO(bookCopyService.returnCopy(id)));
    }

}
