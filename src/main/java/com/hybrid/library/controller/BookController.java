package com.hybrid.library.controller;

import com.hybrid.library.dto.BookCopyDTO;
import com.hybrid.library.dto.BookDTO;
import com.hybrid.library.mapper.BookCopyMapper;
import com.hybrid.library.mapper.BookMapper;
import com.hybrid.library.model.BookCopy;
import com.hybrid.library.service.BookService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/books")
public class BookController {

    private final BookService bookService;

    private final BookMapper bookMapper;

    private final BookCopyMapper bookCopyMapper;

    public BookController(BookService bookService, BookMapper bookMapper, BookCopyMapper bookCopyMapper) {
        this.bookService = bookService;
        this.bookMapper = bookMapper;
        this.bookCopyMapper = bookCopyMapper;
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<BookDTO> createBook(@Valid @RequestBody BookDTO book) {
        return new ResponseEntity<>(bookMapper.mapToBookDTO(bookService.createBook(bookMapper.mapToBook(book))), HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public ResponseEntity<BookDTO> findOneBook(@PathVariable("id") Long id) {
        return ResponseEntity.ok(bookMapper.mapToBookDTO(bookService.findOne(id)));
    }

    @GetMapping
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public ResponseEntity<Page<BookDTO>> findAllBooks(Pageable pageable) {
        return ResponseEntity.ok(bookMapper.mapToBookDTOPage(bookService.getAll(pageable)));
    }

    @DeleteMapping(value = "/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<HttpStatus> removeBook(@PathVariable("id") Long id) {
        bookService.delete(id);
        return ResponseEntity.noContent().build();
    }

    @PutMapping(value = "/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<BookDTO> updateBook(@PathVariable("id") Long id, @Valid @RequestBody BookDTO book) {
        return ResponseEntity.ok(bookMapper.mapToBookDTO(bookService.updateBook(id, bookMapper.mapToBook(book))));
    }

    @GetMapping(value = "/{id}/copies/available")
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    public ResponseEntity <List<BookCopyDTO>>getAvailableBooksCopies(@PathVariable("id") Long id) {
        return ResponseEntity.ok(bookCopyMapper.mapToBookCopyDTOList(bookService.getAvailableBookCopies(id)));
    }

    @PutMapping(value = "/{id}/rent")
    @PreAuthorize("hasRole('USER')")
    public ResponseEntity<BookCopyDTO> rentACopy(@PathVariable("id") Long id) {
        return ResponseEntity.ok(bookCopyMapper.mapToBookCopyDTO(bookService.rentACopy(id)));
    }

}
