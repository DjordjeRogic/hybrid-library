package com.hybrid.library.security;

import com.hybrid.library.configuration.TokenProperties;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;


@Component
public class TokenUtils {

    private final TokenProperties tokenProperties;

    public TokenUtils(TokenProperties tokenProperties) {
        this.tokenProperties = tokenProperties;
    }

    public String generateToken(String email) {
        Date now = new Date();
        return Jwts.builder()
                .setIssuer(tokenProperties.getApplicationName())
                .setSubject(email)
                .setIssuedAt(now)
                .setExpiration(generateExpirationDate(now))
                .signWith(tokenProperties.getSignAlgorithm(), tokenProperties.getSecret()).compact();
    }

    public Date generateExpirationDate(Date date) {
        return new Date(date.getTime() + tokenProperties.getExpiresIn());
    }

    public Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                .setSigningKey(tokenProperties.getSecret())
                .parseClaimsJws(token)
                .getBody();
    }

    public Boolean isTokenValid(String token, UserDetails userDetails) {
        String email = getEmailFromToken(token);
        return (isNotEmpty(email) && email.equals(userDetails.getUsername()));
    }

    public String getEmailFromToken(String token) {
        Claims claims = this.getAllClaimsFromToken(token);
        return claims.getSubject();
    }

    public Date getExpirationDateFromToken(String token) {
        Claims claims = this.getAllClaimsFromToken(token);
        return claims.getExpiration();
    }

    public String getToken(HttpServletRequest request) {
        String authHeader = getAuthHeaderFromHeader(request);
        return authHeader.substring(tokenProperties.getBearerPrefix().length());
    }

    private String getAuthHeaderFromHeader(HttpServletRequest request) {
        return request.getHeader(tokenProperties.getAuthHeader());
    }

    public Boolean isTokenExpired(String token) {
        Date expiration = this.getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    private boolean isNotEmpty(String string){
        return !string.isEmpty();
    }
}
