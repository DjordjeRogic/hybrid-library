package com.hybrid.library.security.authentication;

import com.hybrid.library.security.TokenUtils;
import com.hybrid.library.service.impl.CustomUserDetailsService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class TokenAuthorizationFilter extends OncePerRequestFilter {

    private final CustomUserDetailsService userDetailsService;

    private final TokenUtils tokenUtils;

    public TokenAuthorizationFilter(CustomUserDetailsService userDetailsService, TokenUtils tokenUtils) {
        this.userDetailsService = userDetailsService;
        this.tokenUtils = tokenUtils;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        try {
            String authToken = tokenUtils.getToken(httpServletRequest);
            String email = tokenUtils.getEmailFromToken(authToken);
            UserDetails userDetails = userDetailsService.loadUserByUsername(email);

            if (tokenUtils.isTokenValid(authToken, userDetails)) {

                TokenBasedAuthentication authentication = new TokenBasedAuthentication(userDetails);
                authentication.setToken(authToken);
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        } catch (RuntimeException runtimeException){
            throw runtimeException;
        }

        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }
}