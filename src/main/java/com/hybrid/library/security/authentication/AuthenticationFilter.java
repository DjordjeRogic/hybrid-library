package com.hybrid.library.security.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hybrid.library.configuration.TokenProperties;
import com.hybrid.library.model.User;
import com.hybrid.library.security.TokenUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.stereotype.Component;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthenticationFilter extends AbstractAuthenticationProcessingFilter {

    private final TokenUtils tokenUtils;

    private final ObjectMapper objectMapper;

    private final TokenProperties tokenProperties;

    public AuthenticationFilter(AuthenticationManager authenticationManager, TokenUtils tokenUtils, ObjectMapper objectMapper, TokenProperties tokenProperties) {
        super("/login");
        setAuthenticationManager(authenticationManager);
        this.tokenUtils = tokenUtils;
        this.objectMapper = objectMapper;
        this.tokenProperties = tokenProperties;
    }


    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws AuthenticationException, IOException, ServletException {
        AuthenticationRequest creds = objectMapper.readValue(httpServletRequest.getInputStream(), AuthenticationRequest.class);
        return getAuthenticationManager().authenticate(new UsernamePasswordAuthenticationToken(creds.getUsername(), creds.getPassword()));
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain, Authentication auth) throws IOException, ServletException {
        User userAuth = (User) auth.getPrincipal();
        String jwt = tokenUtils.generateToken(userAuth.getEmail());
        res.addHeader(tokenProperties.getAuthHeader(), tokenProperties.getBearerPrefix() + jwt);
    }

    @Override
    @Autowired
    public void setAuthenticationManager(AuthenticationManager authenticationManager) {
        super.setAuthenticationManager(authenticationManager);
    }
}
