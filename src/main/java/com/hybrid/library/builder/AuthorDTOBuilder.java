package com.hybrid.library.builder;

import com.hybrid.library.dto.AuthorDTO;

public class AuthorDTOBuilder {

    private String firstName;
    private String lastName;
    private String middleName;

    public AuthorDTOBuilder firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public AuthorDTOBuilder lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public AuthorDTOBuilder middleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public AuthorDTO build() {
        AuthorDTO authorDTO = new AuthorDTO();
        authorDTO.setFirstName(this.firstName);
        authorDTO.setLastName(this.lastName);
        authorDTO.setMiddleName(this.middleName);
        return authorDTO;
    }
}
