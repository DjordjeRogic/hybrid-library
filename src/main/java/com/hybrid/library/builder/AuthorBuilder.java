package com.hybrid.library.builder;

import com.hybrid.library.model.Author;

public class AuthorBuilder {

    private String firstName;
    private String lastName;
    private String middleName;

    public AuthorBuilder firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public AuthorBuilder lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public AuthorBuilder middleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public Author build() {
        Author author = new Author();
        author.setFirstName(firstName);
        author.setLastName(lastName);
        author.setMiddleName(middleName);
        return author;
    }
}
