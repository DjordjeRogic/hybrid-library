package com.hybrid.library.exceptionHandling.exception;

public class RentedLimitException extends RuntimeException {

    public RentedLimitException(String message) {
        super(message);
    }
}
