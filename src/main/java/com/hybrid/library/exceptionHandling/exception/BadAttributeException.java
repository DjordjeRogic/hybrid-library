package com.hybrid.library.exceptionHandling.exception;

public class BadAttributeException extends RuntimeException {

    public BadAttributeException(String message){
        super(message);
    }
}
