package com.hybrid.library.exceptionHandling.advice;

import com.hybrid.library.dto.ErrorDTO;
import com.hybrid.library.exceptionHandling.exception.BadAttributeException;
import com.hybrid.library.exceptionHandling.exception.EntityNotFoundException;
import com.hybrid.library.exceptionHandling.exception.RentedLimitException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;
import java.util.List;

@ControllerAdvice
public class ControllerExceptionHandling {

    @ExceptionHandler(value = EntityNotFoundException.class)
    public ResponseEntity<ErrorDTO> notFoundException(EntityNotFoundException exception) {
        return new ResponseEntity<>(createErrorDTO(HttpStatus.NOT_FOUND, exception.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = BadAttributeException.class)
    public ResponseEntity<ErrorDTO> attributeMissingException(BadAttributeException exception) {
        return new ResponseEntity<>(createErrorDTO(HttpStatus.BAD_REQUEST, exception.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<ErrorDTO> attributeMissingException(MethodArgumentNotValidException exception) {
        BindingResult bindingResult = exception.getBindingResult();
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        String errorMessage = fieldErrors.get(0).getDefaultMessage();
        return new ResponseEntity<>(createErrorDTO(HttpStatus.BAD_REQUEST, errorMessage), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = RentedLimitException.class)
    public ResponseEntity<ErrorDTO> rentedLimitException(RentedLimitException exception) {
        return new ResponseEntity<>(createErrorDTO(HttpStatus.BAD_REQUEST, exception.getMessage()), HttpStatus.BAD_REQUEST);
    }

    private ErrorDTO createErrorDTO(HttpStatus httpStatus, String message) {
        return ErrorDTO.builder()
                .status(httpStatus.value())
                .statusDescription(httpStatus.getReasonPhrase())
                .timestamp(LocalDateTime.now())
                .message(message)
                .build();
    }
}
