package com.hybrid.library.repository;

import com.hybrid.library.model.Author;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AuthorRepository extends JpaRepository<Author, Long> {

    Optional<Author> findByFirstNameLikeAndMiddleNameLikeAndLastNameLike(String firstName, String middleName, String lastName);

}
