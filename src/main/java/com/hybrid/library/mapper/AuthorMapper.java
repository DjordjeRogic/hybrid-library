package com.hybrid.library.mapper;

import com.hybrid.library.builder.AuthorBuilder;
import com.hybrid.library.builder.AuthorDTOBuilder;
import com.hybrid.library.dto.AuthorDTO;
import com.hybrid.library.model.Author;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class AuthorMapper {

    public AuthorDTO mapToAuthorDTO(Author author) {
        return new AuthorDTOBuilder()
                .firstName(author.getFirstName())
                .middleName(author.getMiddleName())
                .lastName(author.getLastName())
                .build();
    }

    public Author mapToAuthor(AuthorDTO authorDTO) {
        return new AuthorBuilder()
                .firstName(authorDTO.getFirstName())
                .middleName(authorDTO.getMiddleName())
                .lastName(authorDTO.getLastName())
                .build();
    }

    public Set<AuthorDTO> mapToAuthorDTOSet(Set<Author> authors) {
        return authors.stream()
                .map(this::mapToAuthorDTO)
                .collect(Collectors.toSet());
    }

    public Set<Author> mapToAuthorSet(Set<AuthorDTO> authors) {
        return authors.stream()
                .map(this::mapToAuthor)
                .collect(Collectors.toSet());
    }
}
