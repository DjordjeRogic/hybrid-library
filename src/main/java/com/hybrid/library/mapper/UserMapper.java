package com.hybrid.library.mapper;

import com.hybrid.library.dto.UserDTO;
import com.hybrid.library.model.Role;
import com.hybrid.library.model.User;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class UserMapper {

    public User mapToUser(UserDTO userDTO) {
        return User.builder()
                .id(userDTO.getId())
                .email(userDTO.getEmail())
                .name(userDTO.getName())
                .surname(userDTO.getSurname())
                .password(userDTO.getPassword())
                .roles(userDTO.getRoles().stream()
                        .map(Role::new)
                        .collect(Collectors.toSet()))
                .build();
    }

    public UserDTO mapToUserDTO(User user) {
        return UserDTO.builder()
                .id(user.getId())
                .email(user.getEmail())
                .name(user.getName())
                .surname(user.getSurname())
                .build();
    }

    public Page<UserDTO> mapToUserDTOPage(Page<User> userPage) {
        return userPage.map(this::mapToUserDTO);
    }
}
