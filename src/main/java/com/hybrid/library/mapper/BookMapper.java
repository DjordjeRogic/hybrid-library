package com.hybrid.library.mapper;

import com.hybrid.library.dto.BookDTO;
import com.hybrid.library.model.Book;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

@Component
public class BookMapper {

    private final AuthorMapper authorMapper;

    public BookMapper(AuthorMapper authorMapper) {
        this.authorMapper = authorMapper;
    }

    public Book mapToBook(BookDTO bookDTO) {
        return Book.builder()
                .id(bookDTO.getId())
                .name(bookDTO.getName())
                .authors(authorMapper.mapToAuthorSet(bookDTO.getAuthors()))
                .description(bookDTO.getDescription())
                .build();
    }

    public BookDTO mapToBookDTO(Book book) {
        return BookDTO.builder()
                .id(book.getId())
                .name(book.getName())
                .authors(authorMapper.mapToAuthorDTOSet(book.getAuthors()))
                .description(book.getDescription())
                .build();
    }

    public Page<BookDTO> mapToBookDTOPage(Page<Book> bookPage) {
        return bookPage.map(this::mapToBookDTO);
    }
}
