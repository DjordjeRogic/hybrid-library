package com.hybrid.library.mapper;

import com.hybrid.library.dto.BookCopyDTO;
import com.hybrid.library.dto.UserDTO;
import com.hybrid.library.model.BookCopy;
import com.hybrid.library.model.User;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BookCopyMapper {

    private final BookMapper bookMapper;

    private final UserMapper userMapper;

    public BookCopyMapper(BookMapper bookMapper, UserMapper userMapper) {
        this.bookMapper = bookMapper;
        this.userMapper = userMapper;
    }

    public BookCopy mapToBookCopy(BookCopyDTO bookCopyDTO) {

        User user = bookCopyDTO.isRented() ? userMapper.mapToUser(bookCopyDTO.getUser()) : null;

        return BookCopy.builder()
                .id(bookCopyDTO.getId())
                .copyNumber(bookCopyDTO.getCopyNumber())
                .rentalDate(bookCopyDTO.getRentalDate())
                .book(bookMapper.mapToBook(bookCopyDTO.getBook()))
                .user(user)
                .build();
    }

    public BookCopyDTO mapToBookCopyDTO(BookCopy bookCopy) {


        UserDTO user = bookCopy.isRented() ? userMapper.mapToUserDTO(bookCopy.getUser()) : null;

        return BookCopyDTO.builder()
                .id(bookCopy.getId())
                .copyNumber(bookCopy.getCopyNumber())
                .rentalDate(bookCopy.getRentalDate())
                .book(bookMapper.mapToBookDTO(bookCopy.getBook()))
                .user(user)
                .build();
    }

    public List<BookCopyDTO> mapToBookCopyDTOList(List<BookCopy> copyList) {
        return copyList.stream()
                .map(this::mapToBookCopyDTO)
                .collect(Collectors.toList());
    }
}
