package com.hybrid.library.service;

import com.hybrid.library.model.Book;
import com.hybrid.library.model.BookCopy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BookService {
    /**
     * Saves the book which is passed as parameter.
     * If the book contains id the existing row in the database will be updated
     * If the book doesn't contain id, a new row will be created in the Book table and
     * id will be generated.
     *
     * @param book the book object that needs to be saved
     * @return saved book with given id
     */
    Book save(Book book);

    /**
     * Deletes row from Book table with corresponding id
     *
     * @param id id of the book that will be deleted
     */
    void delete(Long id);

    /**
     * Searches Book table in database for a row with corresponding id.
     * If there isn't row with given id the method will return null
     *
     * @param id id of the book that will be returned
     * @return Book object found in database
     */
    Book findOne(Long id);

    /**
     * Gets all the rows from the Book table specified by page number and size of the page in number of elements.
     *
     * @param pageable Object with number of page and size attributes of the page that should be
     *                 returned.
     * @return Returns page object with book
     */
    Page<Book> getAll(Pageable pageable);

    /**
     * Searches for a book based on id parameter updates the fields and saves it to the database.
     * If the book for given id is not found returns null
     *
     * @param id   Id of the book that will be updated
     * @param book Book object with updated fields
     * @return Updated book or null if the book doesn't exist
     */
    Book updateBook(Long id, Book book);

    List<BookCopy> getAvailableBookCopies(Long id);

    BookCopy rentACopy(Long book_id);

    Book createBook(Book book);

}
