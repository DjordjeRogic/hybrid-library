package com.hybrid.library.service;

import com.hybrid.library.model.User;
import javassist.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface UserService {
    User save(User user);

    User findOne(Long id);

    Page<User> findAll(Pageable pageable);

    void delete(Long id);

    User update(Long id, User user);

    User findByEmail(String email);

    User createNewUser(User user);

    User getLoggedUser();
}
