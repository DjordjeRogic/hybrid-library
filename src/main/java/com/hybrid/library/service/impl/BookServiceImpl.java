package com.hybrid.library.service.impl;

import com.hybrid.library.exceptionHandling.exception.EntityNotFoundException;
import com.hybrid.library.exceptionHandling.exception.RentedLimitException;
import com.hybrid.library.model.Book;
import com.hybrid.library.model.BookCopy;
import com.hybrid.library.model.User;
import com.hybrid.library.repository.BookRepository;
import com.hybrid.library.service.AuthorService;
import com.hybrid.library.service.BookCopyService;
import com.hybrid.library.service.BookService;
import com.hybrid.library.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    private final BookCopyService bookCopyService;

    private final UserService userService;

    private final AuthorServiceImpl authorService;

    private static final Logger logger = LoggerFactory.getLogger(BookServiceImpl.class);

    @Value("${user.rent.limit}")
    private int rentLimit;

    public BookServiceImpl(BookRepository bookRepository, BookCopyService bookCopyService, UserService userService, AuthorServiceImpl authorService) {
        this.bookRepository = bookRepository;
        this.bookCopyService = bookCopyService;
        this.userService = userService;
        this.authorService = authorService;
    }

    @Override
    public Book save(Book book) {
        return bookRepository.save(book);
    }

    @Override
    public void delete(Long id) {
        bookRepository.delete(this.findOne(id));
    }

    @Override
    public Book findOne(Long id) {
        return bookRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Book with id: " + id + " doesn't exist."));
    }

    @Override
    public Page<Book> getAll(Pageable pageable) {
        return bookRepository.findAll(pageable);
    }

    @Override
    public Book updateBook(Long id, Book book) {
        Book updatedBook = this.findOne(id);
        updatedBook.setAuthors(authorService.getAuthors(book.getAuthors()));
        updatedBook.setDescription(book.getDescription());
        updatedBook.setName(book.getName());
        updatedBook = save(updatedBook);
        return updatedBook;
    }

    @Override
    public List<BookCopy> getAvailableBookCopies(Long id) {
        Book book = this.findOne(id);
        List<BookCopy> bookCopies = book.getBookCopies().stream()
                .filter(this::isAvailable)
                .collect(Collectors.toList());
        return bookCopies;
    }

    @Override
    public BookCopy rentACopy(Long book_id) {
        User user = userService.getLoggedUser();

        if (userHasRentLimit(user)) {
            logger.info("User with ID: " + user.getId() + " tried to rent more than the limit: ."+rentLimit);
            throw new RentedLimitException("You already rented maximum number of copies!");
        }

        BookCopy bookCopy = getAvailableBookCopies(book_id).stream()
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("No copies available for this book."));
        bookCopy.setRentalDate(LocalDate.now());
        bookCopy.setUser(user);
        return bookCopyService.save(bookCopy);
    }

    @Override
    public Book createBook(Book book) {
        Book newBook = new Book();
        newBook.setName(book.getName());
        newBook.setDescription(book.getDescription());
        newBook.setAuthors(authorService.getAuthors(book.getAuthors()));
        return save(newBook);
    }

    private boolean isAvailable(BookCopy bookCopy) {
        return Objects.isNull(bookCopy.getRentalDate());
    }

    private boolean userHasRentLimit(User user) {
        return user.getBookCopies().size() > rentLimit;
    }
}
