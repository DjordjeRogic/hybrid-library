package com.hybrid.library.service.impl;

import com.hybrid.library.model.Author;
import com.hybrid.library.repository.AuthorRepository;
import com.hybrid.library.service.AuthorService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;

    public AuthorServiceImpl(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }

    @Override
    public Author addAuthor(Author author) {
        Author newAuthor = new Author();
        newAuthor.setFirstName(author.getFirstName());
        newAuthor.setMiddleName(author.getMiddleName());
        newAuthor.setLastName(author.getLastName());
        return authorRepository.save(newAuthor);
    }

    public Set<Author> getAuthors(Set<Author> authors) {
        Set<Author> newAuthors = new HashSet<>();

        for (Author author : authors) {
            Author authorExist = findAuthor(author).orElseGet(() -> addAuthor(author));
            newAuthors.add(authorExist);
        }

        return newAuthors;
    }

    private Optional<Author> findAuthor(Author author) {
        return authorRepository.findByFirstNameLikeAndMiddleNameLikeAndLastNameLike(author.getFirstName(), author.getMiddleName(), author.getLastName());
    }

}
