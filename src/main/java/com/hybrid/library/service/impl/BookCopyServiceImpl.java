package com.hybrid.library.service.impl;

import com.hybrid.library.exceptionHandling.exception.EntityNotFoundException;
import com.hybrid.library.model.BookCopy;
import com.hybrid.library.model.User;
import com.hybrid.library.repository.BookCopyRepository;
import com.hybrid.library.service.BookCopyService;
import com.hybrid.library.service.UserService;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class BookCopyServiceImpl implements BookCopyService {

    private final BookCopyRepository bookCopyRepository;

    private final UserService userService;

    public BookCopyServiceImpl(BookCopyRepository bookCopyRepository,UserService userService) {
        this.bookCopyRepository = bookCopyRepository;
        this.userService = userService;
    }

    @Override
    public BookCopy returnCopy(Long id) {
        BookCopy bookCopy = bookCopyRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Copy with id: " + id + " not found."));
        User user = userService.getLoggedUser();

        if (userRentedThisBookCopy(bookCopy, user)) {
            return bookCopyRepository.save(setReturnCopy(bookCopy));
        }else {
            throw new EntityNotFoundException("You did not rent this book!");
        }
    }

    @Override
    public BookCopy save(BookCopy bookCopy) {
        return bookCopyRepository.save(bookCopy);
    }

    private boolean userRentedThisBookCopy(BookCopy bookCopy, User user){

        if(Objects.isNull(bookCopy.getUser())) {
            return false;
        }

        return bookCopy.getUser().equals(user);
    }

    private BookCopy setReturnCopy(BookCopy bookCopy){
        bookCopy.setUser(null);
        bookCopy.setRentalDate(null);
        return bookCopy;
    }

}
