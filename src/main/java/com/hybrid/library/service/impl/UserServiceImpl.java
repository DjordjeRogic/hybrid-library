package com.hybrid.library.service.impl;

import com.hybrid.library.exceptionHandling.exception.EntityNotFoundException;
import com.hybrid.library.model.Role;
import com.hybrid.library.model.User;
import com.hybrid.library.repository.RoleRepository;
import com.hybrid.library.repository.UserRepository;
import com.hybrid.library.service.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final RoleRepository roleRepository;

    private final PasswordEncoder passwordEncoder;

    public UserServiceImpl(UserRepository userRepository, RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User save(User user) {
        return userRepository.save(user);
    }

    @Override
    public User findOne(Long id) {
        return userRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("User with id: " + id + " not found."));
    }

    @Override
    public Page<User> findAll(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    public void delete(Long id) {
        userRepository.deleteById(id);
    }

    @Override
    public User update(Long id, User user) {
        User updatedUser = userRepository.getOne(id);
        updatedUser.setName(user.getName());
        updatedUser.setSurname(user.getSurname());
        updatedUser.setEmail(user.getEmail());
        updatedUser = this.save(updatedUser);
        return updatedUser;
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByEmail(email)
                .orElseThrow(() -> new EntityNotFoundException("User with email: " + email + " not found."));
    }

    @Override
    public User createNewUser(User user) {
        User newUser = new User();
        newUser.setEmail(user.getEmail());
        newUser.setSurname(user.getSurname());
        newUser.setName(user.getName());
        newUser.setPassword(passwordEncoder.encode(user.getPassword()));

        List<String> names = user.getRoles().stream()
                .map(Role::getName)
                .collect(Collectors.toList());

        newUser.setRoles(roleRepository.findAllByNameIn(names));
        return save(newUser);
    }

    @Override
    public User getLoggedUser() {
        return this.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
    }
}
