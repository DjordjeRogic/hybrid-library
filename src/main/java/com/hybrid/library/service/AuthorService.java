package com.hybrid.library.service;

import com.hybrid.library.model.Author;

import java.util.Optional;
import java.util.Set;

public interface AuthorService {

    Author addAuthor(Author author);

}
