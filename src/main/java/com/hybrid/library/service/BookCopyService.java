package com.hybrid.library.service;

import com.hybrid.library.model.BookCopy;

public interface BookCopyService {

    BookCopy returnCopy(Long id);

    BookCopy save(BookCopy bookCopy);

}
