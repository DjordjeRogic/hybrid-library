package com.hybrid.library.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@Getter
@Setter
public class AuthorDTO {

    @NotNull(message = "Author must have first name")
    private String firstName;

    @NotNull(message = "Author must have last name")
    private String lastName;

    private String middleName;

}
