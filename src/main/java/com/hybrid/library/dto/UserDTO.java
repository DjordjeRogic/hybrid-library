package com.hybrid.library.dto;

import lombok.*;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    private Long id;

    @NotBlank(message = "User must have a name!")
    private String name;

    @NotBlank(message = "User must have a surname!")
    private String surname;

    @NotBlank(message = "User must have an email!")
    @Email
    private String email;

    @NotBlank(message = "User must have a password!")
    private String password;

    private ArrayList<String> roles;
}
