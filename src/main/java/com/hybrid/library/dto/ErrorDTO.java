package com.hybrid.library.dto;

import lombok.*;

import java.time.LocalDateTime;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ErrorDTO {

    private Integer status;

    private String statusDescription;

    private LocalDateTime timestamp;

    private String message;

}
