package com.hybrid.library.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.Objects;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookCopyDTO {

    private Long id;

    @NotBlank(message = "Copy must have a number!")
    private Integer copyNumber;

    private LocalDate rentalDate;

    private BookDTO book;

    private UserDTO user;

    public boolean isRented() {
        return Objects.nonNull(this.user);
    }
}
