package com.hybrid.library.dto;

import lombok.*;

import javax.validation.constraints.NotBlank;
import java.util.HashSet;
import java.util.Set;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookDTO {

    private Long id;

    @NotBlank(message = "Book must have a name!")
    private String name;

    @NotBlank(message = "Book must have a description!")
    private String description;

    private Set<AuthorDTO> authors = new HashSet<>();

}
