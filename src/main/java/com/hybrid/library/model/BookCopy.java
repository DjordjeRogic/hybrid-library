package com.hybrid.library.model;

import lombok.*;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Objects;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "book_copy")
public class BookCopy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private Integer copyNumber;

    private LocalDate rentalDate;

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private Book book;

    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private User user;

    @Override
    public int hashCode() {
        return Objects.hash(this.getCopyNumber());
    }

    @Override
    public boolean equals(Object obj) {

        if (!(obj instanceof BookCopy)) {
            return false;
        }

        BookCopy other = (BookCopy) obj;

        boolean copyNumberEquals = this.getCopyNumber().equals(other.getCopyNumber());

        return copyNumberEquals;
    }

    public boolean isRented() {
        return Objects.nonNull(this.user);
    }
}
