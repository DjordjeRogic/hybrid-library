package com.hybrid.library.model;

import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;

    private String lastName;

    private String middleName;

    @ManyToMany
    private Set<Book> books = new HashSet<>();

    @Override
    public int hashCode() {
        return Objects.hash(this.getFirstName() + this.getMiddleName() + this.getLastName());
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Author)) {
            return false;
        }

        Author other = (Author) obj;

        boolean firstNameEquals = this.getFirstName().equals(other.getFirstName());

        boolean lastNameEquals = this.getLastName().equals(other.getLastName());


        boolean middleNameEquals;

        if (Objects.isNull(this.getMiddleName()) && Objects.nonNull(other.getMiddleName())) {
            middleNameEquals = false;
        } else {
            middleNameEquals = (Objects.isNull(this.getMiddleName()) && Objects.isNull(other.getMiddleName()))
                    || (this.getMiddleName().equals(other.getMiddleName()));
        }

        return firstNameEquals && lastNameEquals && middleNameEquals;
    }

}
