package com.hybrid.library.unit.service;

import com.hybrid.library.model.Book;
import com.hybrid.library.model.BookCopy;
import com.hybrid.library.repository.BookRepository;
import com.hybrid.library.service.impl.BookCopyServiceImpl;
import com.hybrid.library.service.impl.BookServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BookServiceTest {

    @Mock
    private BookRepository bookRepository;

    @Mock
    private BookCopyServiceImpl bookCopyService;

    @InjectMocks
    private BookServiceImpl bookService;

    @Test
    public void saveBook() {
        Book book = new Book();
        book.setName("Knjiga");
        book.setDescription("Opis");
        when(bookRepository.save(Mockito.eq(book))).then(returnsFirstArg());
        book = bookService.save(book);
        assertThat(book.getName()).isEqualTo("Knjiga");
    }

    @Test
    public void updateBook() {
        Book book = new Book();
        book.setId(1l);
        book.setName("Knjiga");
        book.setDescription("Opis");
        when(bookRepository.findById(Mockito.eq(1l))).thenReturn(Optional.of(book));
        when(bookRepository.save(Mockito.eq(book))).then(returnsFirstArg());

        Book updateBook = new Book();
        updateBook.setName("Izmenjena knjiga");
        book = bookService.updateBook(1l, updateBook);
        assertThat(book.getName()).isEqualTo("Izmenjena knjiga");
    }

    @Test
    public void findOne() {
        Long id = 7l;
        when(bookRepository.findById(Mockito.eq(id))).thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND));
        assertThatThrownBy(() -> bookService.findOne(id)).isInstanceOf(ResponseStatusException.class);
    }

    @Test
    public void rentBookCopy() {
        Book book = new Book();
        BookCopy bookCopy = new BookCopy();
        book.getBookCopies().add(bookCopy);
        when(bookRepository.findById(Mockito.eq(1l))).thenReturn(Optional.of(book));
        when(bookCopyService.save(Mockito.eq(bookCopy))).then(returnsFirstArg());

        bookCopy = bookService.rentACopy(1l);
        assertThat(bookCopy.getRentalDate()).isEqualTo(LocalDate.now());

    }

}
