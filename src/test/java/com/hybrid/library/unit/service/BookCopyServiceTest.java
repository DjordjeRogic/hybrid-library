package com.hybrid.library.unit.service;

import com.hybrid.library.model.BookCopy;
import com.hybrid.library.repository.BookCopyRepository;
import com.hybrid.library.service.impl.BookCopyServiceImpl;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class BookCopyServiceTest {

    @Mock
    private BookCopyRepository bookCopyRepository;

    @InjectMocks
    private BookCopyServiceImpl bookCopyService;

    @Test
    public void returnBookCopy() {
        BookCopy bookCopy = new BookCopy();
        bookCopy.setRentalDate(LocalDate.now()); //Book is rented
        when(bookCopyRepository.findById(Mockito.eq(1l))).thenReturn(Optional.of(bookCopy));
        when(bookCopyRepository.save(Mockito.eq(bookCopy))).then(returnsFirstArg());

        bookCopy = bookCopyService.returnCopy(1l);
        assertThat(bookCopy.getRentalDate()).isNull();

    }
}
