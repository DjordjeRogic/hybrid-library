package com.hybrid.library.model;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class BookCopyTest {

    @Test
    void testEqualsSame() {
        BookCopy bookCopyFirst = BookCopy.builder()
                .copyNumber(12)
                .build();

        BookCopy bookCopySecond = BookCopy.builder()
                .copyNumber(12)
                .build();

        assertThat(bookCopyFirst.equals(bookCopySecond));
    }

    @Test
    void testEqualsDifferent() {
        BookCopy bookCopyFirst = BookCopy.builder()
                .copyNumber(20)
                .build();

        BookCopy bookCopySecond = BookCopy.builder()
                .copyNumber(12)
                .build();

        assertThat(!bookCopyFirst.equals(bookCopySecond));
    }
}