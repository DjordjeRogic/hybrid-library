package com.hybrid.library.model;

import com.hybrid.library.builder.AuthorBuilder;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AuthorTest {

    @Test
    void testEqualsSame() {
        Author firstAuthor = new AuthorBuilder()
                .firstName("Pera")
                .lastName("Peric")
                .middleName("Pera")
                .build();

        Author secondAuthor = new AuthorBuilder()
                .firstName("Pera")
                .lastName("Peric")
                .middleName("Pera")
                .build();

        assertThat(firstAuthor.equals(secondAuthor));
    }

    @Test
    void testEqualsDifferentMiddleName() {
        Author firstAuthor = new AuthorBuilder()
                .firstName("Pera")
                .lastName("Peric")
                .middleName("Djura")
                .build();

        Author secondAuthor = new AuthorBuilder()
                .firstName("Pera")
                .lastName("Peric")
                .middleName("Pera")
                .build();

        assertThat(!firstAuthor.equals(secondAuthor));
    }

    @Test
    void testEqualsDifferentFirstNameSameMiddleName() {
        Author firstAuthor = new AuthorBuilder()
                .firstName("Pera")
                .lastName("Peric")
                .middleName("Pera")
                .build();

        Author secondAuthor = new AuthorBuilder()
                .firstName("Djura")
                .lastName("Peric")
                .middleName("Pera")
                .build();

        assertThat(!firstAuthor.equals(secondAuthor));
    }


    @Test
    void testEqualsMiddleNameNull() {
        Author firstAuthor = new AuthorBuilder()
                .firstName("Pera")
                .lastName("Peric")
                .middleName(null)
                .build();

        Author secondAuthor = new AuthorBuilder()
                .firstName("Djura")
                .lastName("Peric")
                .middleName("Pera")
                .build();

        assertThat(!firstAuthor.equals(secondAuthor));
    }

    @Test
    void testEqualsOtherMiddleNameNull() {
        Author firstAuthor = new AuthorBuilder()
                .firstName("Pera")
                .lastName("Peric")
                .middleName("Djura")
                .build();

        Author secondAuthor = new AuthorBuilder()
                .firstName("Djura")
                .lastName("Peric")
                .middleName(null)
                .build();

        assertThat(!firstAuthor.equals(secondAuthor));
    }

    @Test
    void testEqualsBothMiddleNameNull() {
        Author firstAuthor = new AuthorBuilder()
                .firstName("Pera")
                .lastName("Peric")
                .middleName("Djura")
                .build();

        Author secondAuthor = new AuthorBuilder()
                .firstName("Djura")
                .lastName("Peric")
                .middleName(null)
                .build();

        assertThat(!firstAuthor.equals(secondAuthor));
    }
}