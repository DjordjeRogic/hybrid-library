package com.hybrid.library.dto;

import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

class ErrorDTOTest {

    @Test
    public void build() {
        LocalDateTime currentTime = LocalDateTime.now();
        ErrorDTO errorDTO = ErrorDTO.builder()
                .status(200)
                .timestamp(currentTime)
                .statusDescription("Ok")
                .message("Poruka")
                .build();

        assertThat(errorDTO.getStatus()).isEqualTo(200);
        assertThat(errorDTO.getTimestamp()).isEqualTo(currentTime);
        assertThat(errorDTO.getStatusDescription()).isEqualTo("Ok");
        assertThat(errorDTO.getMessage()).isEqualTo("Poruka");


    }

}