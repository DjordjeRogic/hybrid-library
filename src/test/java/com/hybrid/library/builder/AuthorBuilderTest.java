package com.hybrid.library.builder;

import com.hybrid.library.model.Author;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AuthorBuilderTest {

    @Test
    void TestBuildFirstName() {
        Author author = new AuthorBuilder()
                .firstName("Pera")
                .build();

        assertThat(author.getFirstName()).isEqualTo("Pera");
        assertThat(author.getMiddleName()).isNull();
        assertThat(author.getLastName()).isNull();

    }

    @Test
    void TestBuildLastName() {
        Author author = new AuthorBuilder()
                .lastName("Peric")
                .build();

        assertThat(author.getFirstName()).isNull();
        assertThat(author.getMiddleName()).isNull();
        assertThat(author.getLastName()).isEqualTo("Peric");

    }

    @Test
    void TestBuildMiddleName() {
        Author author = new AuthorBuilder()
                .middleName("Pera")
                .build();

        assertThat(author.getFirstName()).isNull();
        assertThat(author.getMiddleName()).isEqualTo("Pera");
        assertThat(author.getLastName()).isNull();

    }

    @Test
    void TestBuildAll() {
        Author author = new AuthorBuilder()
                .firstName("Pera")
                .middleName("Pera")
                .lastName("Peric")
                .build();

        assertThat(author.getFirstName()).isEqualTo("Pera");
        assertThat(author.getMiddleName()).isEqualTo("Pera");
        assertThat(author.getLastName()).isEqualTo("Peric");

    }
}