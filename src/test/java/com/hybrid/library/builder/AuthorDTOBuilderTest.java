package com.hybrid.library.builder;

import com.hybrid.library.dto.AuthorDTO;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class AuthorDTOBuilderTest {

    @Test
    public void testBuildAll() {
        AuthorDTO authorDTO = new AuthorDTOBuilder()
                .firstName("Pera")
                .middleName("Djura")
                .lastName("Peric")
                .build();

        assertThat(authorDTO.getFirstName()).isEqualTo("Pera");
        assertThat(authorDTO.getMiddleName()).isEqualTo("Djura");
        assertThat(authorDTO.getLastName()).isEqualTo("Peric");
    }

    @Test
    public void testBuildFirstName() {
        AuthorDTO authorDTO = new AuthorDTOBuilder()
                .firstName("Pera")
                .build();

        assertThat(authorDTO.getFirstName()).isEqualTo("Pera");
        assertThat(authorDTO.getMiddleName()).isNull();
        assertThat(authorDTO.getLastName()).isNull();
    }

    @Test
    public void testBuildMiddleName() {
        AuthorDTO authorDTO = new AuthorDTOBuilder()
                .middleName("Djura")
                .build();

        assertThat(authorDTO.getFirstName()).isNull();
        assertThat(authorDTO.getMiddleName()).isEqualTo("Djura");
        assertThat(authorDTO.getLastName()).isNull();
    }

    @Test
    public void testBuildLastName() {
        AuthorDTO authorDTO = new AuthorDTOBuilder()
                .lastName("Peric")
                .build();

        assertThat(authorDTO.getFirstName()).isNull();
        assertThat(authorDTO.getMiddleName()).isNull();
        assertThat(authorDTO.getLastName()).isEqualTo("Peric");
    }
}